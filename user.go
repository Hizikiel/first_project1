package model

type User struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Address  string `json:"address"`
	Email    string `json:"email"`
}
