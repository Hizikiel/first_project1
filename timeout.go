package middleware

import (
	"context"
	"net/http"
	"time"
)

func Timeout(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(r.Context(), 5*time.Second)
		defer cancel()

		done := make(chan struct{})
		go func() {
			defer close(done)
			next.ServeHTTP(w, r.WithContext(ctx))
		}()

		select {
		case <-done:
			return
		case <-ctx.Done():
			if ctx.Err() == context.DeadlineExceeded {
				w.Write([]byte("Request timeout"))
				return
			}
		}
	}
}
