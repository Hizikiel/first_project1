package handlers

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"
)

func UploadImageHandler(w http.ResponseWriter, r *http.Request) {
	err := r.ParseMultipartForm(32 << 20)
	if err != nil {
		http.Error(w, "Unable to parse multipart form", http.StatusBadRequest)
		return
	}
	file, handler, err := r.FormFile("image")
	if err != nil {
		http.Error(w, "Unable to get image from form", http.StatusBadRequest)
		return
	}
	defer file.Close()

	fileName := strconv.FormatInt(time.Now().Unix(), 10) + "-" + handler.Filename

	f, err := os.Create("./uploads/" + fileName)
	if err != nil {
		http.Error(w, "Unable to create file for writing", http.StatusInternalServerError)
		return
	}
	defer f.Close()
	_, err = io.Copy(f, file)
	if err != nil {
		http.Error(w, "Unable to write file to disk", http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(w, "Image uploaded successfully. Filename: %s", fileName)
}
