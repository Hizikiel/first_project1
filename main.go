package main

import (
	"fmt"
	"log"
	"net/http"
	"project1/handlers"
	"project1/middleware"
)

func main() {
	http.HandleFunc("/register", middleware.Timeout(handlers.RegisterHandler))
	http.HandleFunc("/login", middleware.Timeout(handlers.LoginHandler))
	http.HandleFunc("/user", middleware.Timeout(handlers.ListUsersHandler))
	http.HandleFunc("/upload", middleware.Timeout(handlers.UploadImageHandler))
	http.HandleFunc("/open", middleware.Timeout(handlers.OpenImageHandler))
	// Start the HTTP server on port 8080
	fmt.Println("the server is running on port 8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
