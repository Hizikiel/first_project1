package handlers

import (
	"net/http"
	"path/filepath"
)

func OpenImageHandler(w http.ResponseWriter, r *http.Request) {
	fileName := r.URL.Query().Get("filename")
	filePath := filepath.Join("./uploads/", fileName)

	// Open the file
	// f, err := os.Open(filePath)
	// if err != nil {
	//     http.Error(w, "Unable to open file", http.StatusInternalServerError)
	//     return
	// }
	// defer f.Close()

	// Serve the file
	http.ServeFile(w, r, filePath)
}
